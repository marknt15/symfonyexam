<?php
/**
 * Senior PHP Developer Applicant: Mark Edward Tan
 * LinkedIn Profile: www.linkedin.com/in/marknt15
 */

/*
===================
1. Installed in MAMP PRO using:

composer create-project symfony/framework-standard-edition symfonyexam "2.8.*"

2. Created virtual host for testing: http://symfonyexam.dev/

3. Edited symfonyexam/web/.htaccess to make this work:
http://symfonyexam.dev/customer

4. Created Database named “symfonyexam” and table “Customer”
CREATE TABLE `Customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `object_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_recorded` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_784FEC5F232D562B` (`object_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

5.
$ php app/console generate:doctrine:entity
The Entity shortcut name: AppBundle:Customer

// create the table
$ php app/console doctrine:schema:update --force

*/

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

// Custom Added below for API server to work
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AppBundle\Entity\Customer;
use Symfony\Component\HttpFoundation\Response;

class CustomerController extends Controller
{
    function __construct() {
        header("Access-Control-Allow-Orgin: *");
        header("Access-Control-Allow-Methods: *");
        header("Content-Type: application/json");
    }

    /**
     * @Route("/customer", name="customer_get")
     * @Method("GET")
     */
    public function apiGetCustomerAction(Request $request)
    {
        // generate 15 characters random unique string
        $object_id  = $this->uniqueKey(15);
        $records    = $this->getDoctrine()->getRepository('AppBundle:Customer')->findAll();
        return new JsonResponse($records);
    }

    /**
     * @Route("/customer", name="customer_post")
     * @Method("POST")
     */
    public function apiPostCustomer(Request $request)
    {
        // generate 15 characters random unique string
        $object_id  = $this->uniqueKey(15);
        $first_name = $this->sanitize_input($request->get('first_name'));
        $last_name  = $this->sanitize_input($request->get('last_name'));

        $c = new Customer;
        $c->setFirstName($first_name);
        $c->setLastName($last_name);
        $c->setObjectId($object_id);
        $c->setDateRecorded(date('Y-m-d H:i:s'));
        $em = $this->getDoctrine()->getManager();
        $em->persist($c);
        $em->flush();

        $arr = array('first_name' => $first_name, 'last_name' => $last_name);
        return new JsonResponse($arr);
    }

    /**
     * @Route("/customer/{id}", name="customer_delete")
     * @Method("DELETE")
     */
    public function apiDeleteCustomer($id)
    {

        $c  = $this->getDoctrine()->getRepository('AppBundle:Customer')->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($c);
        $em->flush();

        return new Response();
    }

    /**
     * @Route("/customer/{id}", name="customer_update")
     * @Method("PUT")
     */
    public function apiPutCustomer($id, Request $request)
    {
        $first_name = $this->sanitize_input($request->headers->get("first_name"));
        $last_name  = $this->sanitize_input($request->headers->get("last_name"));

        $em = $this->getDoctrine()->getManager();
        $c = $em->getRepository('AppBundle:Customer')->find($id);
        $c->setFirstName($first_name);
        $c->setLastName($last_name);

        $em->flush();

        $arr = array('first_name' => $first_name, 'last_name' => $last_name);
        return new JsonResponse($arr);
    }

    public function sanitize_input($str = '') {
        return mysql_real_escape_string(trim(strip_tags($str)));
    }

    public function uniqueKey($length) {
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }
        return $key;
    }

}
