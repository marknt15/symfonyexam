<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Customer
 *
 * @ORM\Table(name="Customer")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CustomerRepository")
 */
class Customer
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=50)
     */
    public $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=50)
     */
    public $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="object_id", type="string", length=100, unique=true)
     */
    public $objectId;

    /**
     * @var \DateTime
     *
     *  @ORM\Column(name="date_recorded", type="datetime", nullable=true)
     */
    private $dateRecorded;
    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return Customer
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return Customer
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set objectId
     *
     * @param string $objectId
     * @return Customer
     */
    public function setObjectId($objectId)
    {
        $this->objectId = $objectId;

        return $this;
    }

    /**
     * Get objectId
     *
     * @return string
     */
    public function getObjectId()
    {
        return $this->objectId;
    }

    /**
     * Set dateRecorded
     *
     * @param \DateTime $dateRecorded
     * @return Customer
     */
    public function setDateRecorded($dateRecorded)
    {
        //$this->dateRecorded = $dateRecorded;
        $this->dateRecorded = new \DateTime();

        return $this;
    }

    /**
     * Get dateRecorded
     *
     * @return \DateTime
     */
    public function getDateRecorded()
    {
        return $this->dateRecorded;
    }
}
